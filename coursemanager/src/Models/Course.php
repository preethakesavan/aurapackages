<?php

namespace AuraPackages\CourseManager\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends AuraModel
{
    protected $table = 'courses';

    protected $fillable = array('title','subtitle','description', 'instructor_name', 'duration', 'published', 'price', 'logo_id','category_id','subcategory_id', 'is_active');

    public static function boot()
    {
        parent::boot();
    }
}
