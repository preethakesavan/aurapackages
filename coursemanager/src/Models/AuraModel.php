<?php

namespace AuraPackages\CourseManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class AuraModel extends Model
{
    /**
     * Model contructuor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

    }

    public static function boot()
    {
        parent::boot();
    }
}